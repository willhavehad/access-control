local WHITELIST = "Whitelist"
local BLACKLIST = "Blacklist"
local ACMODES = {}
local ACTAB = {}

function parse(str, value)
	if str and str ~= "" then
		print(value .. ": " .. str)
		ACMODES[value] = true
		local entries = str:gmatch("[^ ]+")
		if entries then
			for v in entries do
				ACTAB[v] = value
			end
		end
	end
end

parse(GetConvar("connection_whitelist", ""), WHITELIST)
parse(GetConvar("connection_blacklist", ""), BLACKLIST)

if ACMODES[WHITELIST] then
	if ACMODES[BLACKLIST] then
		print("Whitelist and blacklist loaded.")
	else
		print("Whitelist loaded.")
	end
else
	if ACMODES[BLACKLIST] then
		print("Blacklist loaded.")
	else
		print("No access control.")
	end
end

AddEventHandler("playerConnecting", function(name, setCallback)
	local identifiers = GetPlayerIdentifiers(source)
	local whitelisted = false
	local blacklisted = false
	for k,v in pairs(identifiers) do
		ac = ACTAB["identifier." .. v]
		if ac == WHITELIST then
			whitelisted = true
			print(name .. " whitelisted by identifier " .. v)
		elseif ac == BLACKLIST then
			blacklisted = true
			print(name .. " blacklisted by identifier " .. v)
		end
	end

	if (ACMODES[WHITELIST] and not whitelisted) then
		setCallback("You're not on the guest list.")
		CancelEvent()
		print(name .. " kicked: not whitelisted.")
		return
	end
	if blacklisted then
		setCallback("You're not allowed here.")
		CancelEvent()
		print(name .. " kicked: blacklisted.")
		return
	end

	print(name .. " allowed connection.")
end)
